/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session23)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.

	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 

//Read

	>> Find all regular/non-admin users.

//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.

//Delete

	>> Delete all inactive courses


//Add all of your query/commands here in activity.js
*/

// Create session23 database
use('session23');

// Create session23.users and session23.courses collections
db.createCollection('users');
db.createCollection('courses');

// Insert 5 records in session23.users
db.users.insertMany([
	{ 'firstName': 'Red', 'lastName': 'Ranger', 'email': 'redranger@gmail.com', 'password': 'B5#bTM$i', 'isAdmin': false },
	{ 'firstName': 'Blue', 'lastName': 'Ranger', 'email': 'blueranger@gmail.com', 'password': 'swYd^u2S', 'isAdmin': false },
	{ 'firstName': 'Yellow', 'lastName': 'Ranger', 'email': 'yellowranger@gmail.com', 'password': 'xe$525rB', 'isAdmin': false },
	{ 'firstName': 'White', 'lastName': 'Ranger', 'email': 'whiteranger@gmail.com', 'password': 'TP9jN^dE', 'isAdmin': false },
	{ 'firstName': 'Black', 'lastName': 'Ranger', 'email': 'blackranger@gmail.com', 'password': '3$YX*Dgb', 'isAdmin': false }
]);

// Insert 3 records in session23.courses
db.courses.insertMany([
	{ 'name': 'Introduction to HTML & CSS', 'price': 100, 'isActive': false },
	{ 'name': 'Introduction to JavaScript', 'price': 200, 'isActive': false },
	{ 'name': 'Introduction to MongoDB', 'price': 300, 'isActive': false }
]);

// Find all regular/non-admin users
db.users.find(
	{ 'isAdmin': false }
);

// Update the first record in session23.users as an admin
db.users.updateOne(
	{}, {
	$set: { 'isAdmin': true }
});

// Update the first record in session23.courses as active
db.courses.updateOne(
	{}, {
	$set: { 'isActive': true }
});

// Delete all inactive courses
db.courses.deleteMany(
	{ 'isActive': false }
);
